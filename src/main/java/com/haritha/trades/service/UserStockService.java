package com.haritha.trades.service;

import com.haritha.trade.dto.BuyRequestDto;
import com.haritha.trade.dto.ResponseDto;
import com.haritha.trade.dto.UserStocksResponseDto;

public interface UserStockService {

	public ResponseDto buyProduct(Long userId, BuyRequestDto buyRequestDto);
	public UserStocksResponseDto getStockHistory(Long userId);
	
}