package com.haritha.trades.service;

import com.haritha.trade.dto.StockDetailsDto;
import com.haritha.trade.dto.StockListResponseDto;

public interface StockService {

	public StockListResponseDto findByName(String name);
	public StockDetailsDto findByStockId(Long stockId);
	
}
