package com.haritha.trades.service;

public interface UserService {

	public boolean findUserExistsById(Long id);
	
}
