package com.haritha.trade.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.haritha.trade.dto.UserStockDto;
import com.haritha.trade.entity.User;
import com.haritha.trade.entity.UserStock;

@Repository
public interface UserStockRepository extends CrudRepository<UserStock, Long>{
	
	@Query("select new com.haritha.trade.dto.UserStockDto(s.userstockId,s.stock,s.user,s.quantity,s.price,s.date) from UserStock s WHERE s.user = ?1")
	List<UserStockDto> findByUser(User user);
}
