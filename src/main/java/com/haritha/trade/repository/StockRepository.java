package com.haritha.trade.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.haritha.trade.dto.StockDetailsDto;
import com.haritha.trade.dto.StockDto;
import com.haritha.trade.entity.Stock;

@Repository
public interface StockRepository extends CrudRepository<Stock, Long> {

	@Query("select new com.haritha.trade.dto.StockDto(s.stockId,s.name) from Stock s WHERE s.name LIKE ?1")
	public List<StockDto> findByName(String name);
	
	@Query("select new com.haritha.trade.dto.StockDetailsDto(s.name,s.price,s.quantity,s.date) from Stock s WHERE s.stockId=?1")
	public StockDetailsDto findByStockId(Long id);
	
}
