package com.haritha.trade.repository;

import org.springframework.data.repository.CrudRepository;

import com.haritha.trade.entity.User;

public interface UserRepository extends CrudRepository<User, Long>{

}
