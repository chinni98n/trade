package com.haritha.trade.dto;

import lombok.Data;

@Data
public class ResponseDto {

	private Integer status;
	private String message;
}
