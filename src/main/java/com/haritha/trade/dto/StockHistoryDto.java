package com.haritha.trade.dto;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StockHistoryDto {

	private Long userStockId;
	private String name;
	private Double price;
	private Long quantity;
	private LocalDate date;
	private Double grandPrice;
	
}
