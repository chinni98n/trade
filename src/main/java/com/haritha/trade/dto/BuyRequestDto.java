package com.haritha.trade.dto;

import javax.validation.constraints.Min;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BuyRequestDto {

	@Min(value = 1,message = "{Min.message}")
	private Long stockId;
	
	@Min(value = 1,message = "{Min.message}")
	private Long quantity;
	
}
