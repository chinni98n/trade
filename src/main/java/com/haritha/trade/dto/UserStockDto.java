package com.haritha.trade.dto;

import java.time.LocalDate;

import com.haritha.trade.entity.Stock;
import com.haritha.trade.entity.User;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class UserStockDto {
	
	private Long userstockId;

	private Stock stock;

	private User user;

	private Long quantity;

	private Double unitPrice;

	private LocalDate date;
}
