package com.haritha.trade.dto;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StockDetailsDto {

	private String name;
	private Double price;
	private Long quantity;
	private LocalDate date;
}
