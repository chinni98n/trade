package com.haritha.trade.dto;

import java.util.List;

import lombok.Data;

@Data
public class StockListResponseDto {

	private List<StockDto> stockList;
}
