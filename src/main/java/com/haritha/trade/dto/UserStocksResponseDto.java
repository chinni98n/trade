package com.haritha.trade.dto;

import java.util.List;

import lombok.Data;

@Data
public class UserStocksResponseDto {

	List<StockHistoryDto> stockList;
}
