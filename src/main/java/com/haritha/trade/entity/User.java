package com.haritha.trade.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long userId;

	@NotNull(message = "{NotNull.name}")
	private String name;

	@NotNull(message = "{NotNull.email}")
	@Pattern(regexp = "^[a-zA-Z0-9_.+-]+@(?:(?:[a-zA-Z0-9-]+\\.)?[a-zA-Z]+\\.)?(hcl)\\.com$", message = "{Pattern.email}")
	private String email;

	@NotNull(message = "{NotNull.password}")
	@Pattern(regexp = "	^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,}$\r\n", message = "{Pattern.password}")
	private String password;

	@NotNull(message = "{NotNull.mobile}")
	@Pattern(regexp = "\\d{10}", message = "{Pattern.mobile}")
	private String mobile;

}
