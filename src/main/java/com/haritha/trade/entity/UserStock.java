package com.haritha.trade.entity;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class UserStock {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long userstockId;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "fk_stock_id")
	private Stock stock;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "fk_user_id")
	private User user;
	

	@DecimalMin(value="0.00", message ="{DecimalMin.message}")
	@Digits(integer = 3, fraction = 2)
	private Double price;
	
	@Min(value = 0, message ="{Min.message}")
	private Long quantity;
	
	
	@NotNull(message ="{NotNull.date}")
	private LocalDate date;
}
