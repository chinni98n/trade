package com.haritha.trade.constants;

public class ApplicationConstants {

	private ApplicationConstants() {

	}

	public static final Integer BAD_REQUEST_CODE = 604;
	public static final String BAD_REQUEST = "Bad Request";
	
	public static final Integer NO_RECORD_FOUND_CODE = 605;
	public static final String NO_RECORD_FOUND = "No record found";
	
	public static final Integer SUCCESS_CODE = 606;
	public static final String SUCCESS = "Data saved successfully";
	
	public static final Integer INTERNAL_SERVER_ERROR_CODE = 607;
	public static final String INTERNAL_SERVER_ERROR = "Internal server error";
	
	public static final Integer INSUFFICIENT_STOCKS_CODE = 608;
	public static final String INSUFFICIENT_STOCKS = "Required quantity is not available";

	
}
