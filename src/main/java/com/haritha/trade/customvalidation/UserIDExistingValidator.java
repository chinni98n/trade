package com.haritha.trade.customvalidation;

import java.util.Objects;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.haritha.trade.service.impl.UserServiceImpl;

@Component
public class UserIDExistingValidator implements ConstraintValidator<UserIDExisting, Long>{

	@Autowired
	UserServiceImpl userService;
	
	@Override
	public boolean isValid(Long id, ConstraintValidatorContext context) {
		return Objects.isNull(id)||userService.findUserExistsById(id);
	}

}
