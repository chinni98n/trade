package com.haritha.trade.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.haritha.trade.repository.UserRepository;
import com.haritha.trades.service.UserService;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	UserRepository userRepository;
	
	/**
	 * 
	 * @author : Haritha N
	 * 
	 *         This method is used to check if user exists for a particular id
	 * 
	 * @param id : Long
	 * 
	 *               takes userId as param
	 * 
	 * 
	 * @return boolean
	 * 
	 *         returns status true if User exists with given id else false
	 * 
	 */
	
	@Override
	public boolean findUserExistsById(Long id) {		
		return userRepository.existsById(id);
	}

}
