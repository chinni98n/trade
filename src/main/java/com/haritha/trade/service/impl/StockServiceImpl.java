package com.haritha.trade.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.haritha.trade.constants.ApplicationConstants;
import com.haritha.trade.dto.StockDetailsDto;
import com.haritha.trade.dto.StockDto;
import com.haritha.trade.dto.StockListResponseDto;
import com.haritha.trade.exception.RecordNotFoundException;
import com.haritha.trade.repository.StockRepository;
import com.haritha.trades.service.StockService;

@Service
public class StockServiceImpl implements StockService{

	@Autowired
	StockRepository stockRepository;
	
	/**
	 * 
	 * @author : Haritha N
	 * 
	 *         This method is used to search stock by name
	 * 
	 * @param name : String
	 * 
	 *               takes name as param
	 * 
	 * 
	 * @return StockListResponseDto
	 * 
	 *         returns the list of stocks matching with name
	 * 
	 */
	@Override
	public StockListResponseDto findByName(String name) {
		String name1 = new StringBuilder("%").append(name).append("%").toString();
		List<StockDto> stockDtoList = stockRepository.findByName(name1);
		if(stockDtoList.isEmpty())
			throw new RecordNotFoundException(ApplicationConstants.NO_RECORD_FOUND);
		StockListResponseDto stockList = new StockListResponseDto();
		stockList.setStockList(stockDtoList);
		return stockList;
	}
	
	/**
	 * 
	 * @author : Haritha N
	 * 
	 *         This method is used to get details of a particular stock
	 * 
	 * @param stockId : Long
	 * 
	 *               takes stockId as param
	 * 
	 * 
	 * @return stockDetailsDto
	 * 
	 *         returns the details of particular stock 
	 * 
	 */

	@Override
	public StockDetailsDto findByStockId(Long stockId) {
		StockDetailsDto stockDetails = stockRepository.findByStockId(stockId);
		if(stockDetails==null)
			throw new RecordNotFoundException(ApplicationConstants.NO_RECORD_FOUND);
		return stockDetails;
	}

}
