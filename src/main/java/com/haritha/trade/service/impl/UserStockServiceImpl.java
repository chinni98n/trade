package com.haritha.trade.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.haritha.trade.constants.ApplicationConstants;
import com.haritha.trade.dto.BuyRequestDto;
import com.haritha.trade.dto.ResponseDto;
import com.haritha.trade.dto.StockHistoryDto;
import com.haritha.trade.dto.UserStocksResponseDto;
import com.haritha.trade.dto.UserStockDto;
import com.haritha.trade.entity.Stock;
import com.haritha.trade.entity.User;
import com.haritha.trade.entity.UserStock;
import com.haritha.trade.exception.InsufficientStocksException;
import com.haritha.trade.exception.RecordNotFoundException;
import com.haritha.trade.repository.StockRepository;
import com.haritha.trade.repository.UserRepository;
import com.haritha.trade.repository.UserStockRepository;
import com.haritha.trades.service.UserStockService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UserStockServiceImpl implements UserStockService {

	@Autowired
	UserStockRepository userStockRepository;

	@Autowired
	StockRepository stockRepository;

	@Autowired
	UserRepository userRepository;

	/**
	 * 
	 * @author : Haritha N
	 * 
	 *         This method is used to purchase stock
	 * 
	 * @param userId : Long
	 * 
	 *               takes userId as param
	 * 
	 * @param :      stockId
	 * 
	 * @param :      quantity
	 * 
	 * @return ResponseDto
	 * 
	 *         returns the success response
	 * 
	 */
	
	@Override
	public ResponseDto buyProduct(Long userId, BuyRequestDto buyRequestDto) {

		log.info("Purchase Product");
		Optional<Stock> stockOpt = stockRepository.findById(buyRequestDto.getStockId());
		if (!stockOpt.isPresent()) {
			log.error("Stock object is not existing for stockId : {}", buyRequestDto.getStockId(),
					RecordNotFoundException.class);
			throw new RecordNotFoundException(ApplicationConstants.NO_RECORD_FOUND);
		}
		Stock stock = stockOpt.get();

		if (stock.getQuantity() < buyRequestDto.getQuantity())
			throw new InsufficientStocksException(ApplicationConstants.INSUFFICIENT_STOCKS);

		Optional<User> userOpt = userRepository.findById(userId);
		if (!userOpt.isPresent())
			throw new RecordNotFoundException(ApplicationConstants.NO_RECORD_FOUND);
		User user = userOpt.get();
		UserStock userStock = new UserStock();
		BeanUtils.copyProperties(stock, userStock);
		stock.setQuantity(stock.getQuantity() - buyRequestDto.getQuantity());

		userStock.setStock(stock);
		userStock.setUser(user);
		userStock.setQuantity(buyRequestDto.getQuantity());

		userStockRepository.save(userStock);

		ResponseDto responseDto = new ResponseDto();
		responseDto.setMessage(ApplicationConstants.SUCCESS);
		responseDto.setStatus(ApplicationConstants.SUCCESS_CODE);

		return responseDto;
	}

	/**
	 *
	 * @param userId : Long
	 *
	 *               Here we are taking the stockName from the user
	 *
	 * @return UserStockResponseDto
	 *
	 * @param stockList :List
	 *
	 *                  Here we are return the list of StockHistory of a user
	 */

	public UserStocksResponseDto getStockHistory(Long userId) {

		Optional<User> userOpt = userRepository.findById(userId);
		if (!userOpt.isPresent())
			throw new RecordNotFoundException(ApplicationConstants.NO_RECORD_FOUND);
		User user = userOpt.get();

		List<UserStockDto> userStockDtoList = userStockRepository.findByUser(user);

		if (userStockDtoList.isEmpty())
			throw new RecordNotFoundException(ApplicationConstants.NO_RECORD_FOUND);

		List<StockHistoryDto> stockHistoryDtoList = userStockDtoList.stream()
				.map(userStockDto -> new StockHistoryDto(userStockDto.getUserstockId(),
						userStockDto.getStock().getName(), userStockDto.getStock().getPrice(),
						userStockDto.getQuantity(), userStockDto.getDate(),
						(userStockDto.getStock().getPrice()) * (userStockDto.getQuantity())))
				.collect(Collectors.toList());

		UserStocksResponseDto stockList = new UserStocksResponseDto();
		stockList.setStockList(stockHistoryDtoList);

		return stockList;
	}

}
