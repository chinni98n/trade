package com.haritha.trade.exception;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.haritha.trade.constants.ApplicationConstants;

@RestControllerAdvice
public class ExceptionHandlerControllerAdvice extends ResponseEntityExceptionHandler {

	@ExceptionHandler(Exception.class)
	public ExceptionResponse handleException(final Exception exception, final HttpServletRequest request) {

		ExceptionResponse error = new ExceptionResponse();
		error.setErrorMessage(ApplicationConstants.INTERNAL_SERVER_ERROR);
		error.setStatus(ApplicationConstants.INTERNAL_SERVER_ERROR_CODE);
		return error;
	}

	@ExceptionHandler(ConstraintViolationException.class)
	public final ResponseEntity<Object> handleConstraintViolationException(ConstraintViolationException exception,
			WebRequest request) {

		ExceptionResponse error = new ExceptionResponse();
		error.setErrorMessage(exception.getMessage());
		error.setStatus(ApplicationConstants.BAD_REQUEST_CODE);
		return ResponseEntity.badRequest().body(error);

	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {

		Map<String, Object> body = new LinkedHashMap<>();
		body.put("status", status.value());
		List<String> errors = ex.getBindingResult().getFieldErrors().stream().map(x -> x.getDefaultMessage())
				.collect(Collectors.toList());
		body.put("message", errors);
		return new ResponseEntity<>(body, status);
		
	}

	@ExceptionHandler(RecordNotFoundException.class)
	public final ExceptionResponse handleRecordNotFoundException(Exception exception, WebRequest request) {
		ExceptionResponse error = new ExceptionResponse();
		error.setErrorMessage(exception.getMessage());
		error.setStatus(ApplicationConstants.NO_RECORD_FOUND_CODE);
		return error;

	}
	
	@ExceptionHandler(InsufficientStocksException.class)
	public final ExceptionResponse handleInsufficientStocksException(Exception exception, WebRequest request) {
		ExceptionResponse error = new ExceptionResponse();
		error.setErrorMessage(exception.getMessage());
		error.setStatus(ApplicationConstants.INSUFFICIENT_STOCKS_CODE);
		return error;

	}
}
