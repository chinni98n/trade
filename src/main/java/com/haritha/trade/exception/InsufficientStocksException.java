package com.haritha.trade.exception;

public class InsufficientStocksException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public InsufficientStocksException() {
		super();
	}

	public InsufficientStocksException(final String message) {
		super(message);
	}
}
