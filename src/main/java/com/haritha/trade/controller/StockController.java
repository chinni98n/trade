package com.haritha.trade.controller;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.haritha.trade.dto.StockDetailsDto;
import com.haritha.trade.dto.StockListResponseDto;
import com.haritha.trade.service.impl.StockServiceImpl;

@RestController
@Validated
public class StockController {

	@Autowired
	StockServiceImpl stockService;

	/**
	 * 
	 * @author : Haritha N
	 * 
	 * 
	 * @param name : String
	 * 
	 *             we take name as param from user
	 * 
	 * @return StockListResponseDto
	 * 
	 *         Here we are return the list of Stock names matching the name
	 * 
	 */
	@GetMapping("/stocks")
	public ResponseEntity<StockListResponseDto> getStocksbyName(@RequestParam @NotNull String name) {
		StockListResponseDto stockList = stockService.findByName(name);
		return new ResponseEntity<>(stockList, HttpStatus.OK);
	}

	/**
	 * 
	 * @author : Haritha N
	 * 
	 * @param stockId : Long
	 * 
	 *                takes name as param from user
	 * 
	 * @return StockDetailsDto
	 * 
	 *         returns the details of stock selected
	 * 
	 */
	@GetMapping("/stocks/{stockId}")
	public ResponseEntity<StockDetailsDto> getStockDetails(@PathVariable @Min(1) Long stockId) {
		StockDetailsDto stockDetails = stockService.findByStockId(stockId);
		return new ResponseEntity<>(stockDetails, HttpStatus.OK);
	}

}
