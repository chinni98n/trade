package com.haritha.trade.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.haritha.trade.customvalidation.UserIDExisting;
import com.haritha.trade.dto.BuyRequestDto;
import com.haritha.trade.dto.ResponseDto;
import com.haritha.trade.dto.UserStocksResponseDto;
import com.haritha.trade.service.impl.UserStockServiceImpl;

@RestController
@Validated
public class UserStockController {

	@Autowired
	UserStockServiceImpl userstockService;

	/**
	 * 
	 * @author : Haritha N
	 * 
	 *         This method is used to purchase stock
	 * 
	 * @param userId : Long
	 * 
	 *               takes userId as param
	 * 
	 * @param :      stockId
	 * 
	 * @param :      quantity
	 * 
	 * @return ResponseDto
	 * 
	 *         returns the success response
	 * 
	 */

	@PostMapping("/users/{userId}/stocks")
	public ResponseEntity<ResponseDto> buyStock(@PathVariable("userId") @UserIDExisting Long userId,
			@RequestBody @Valid BuyRequestDto buyRequestDto) {
		ResponseDto response = userstockService.buyProduct(userId, buyRequestDto);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	/**
	 * 
	 * @author : Haritha N
	 * 
	 *         This method is used to get the purchased stocks
	 * 
	 * @param userId : Long
	 * 
	 *               takes userId as param
	 * 
	 * @return UserStocksResponseDto
	 * 
	 * @param List<StockHistoryDto>
	 * 
	 *                              returns the list of purchased stocks of user
	 * 
	 */
	@GetMapping("/users/{userId}/stocks")
	public ResponseEntity<UserStocksResponseDto> myStocks(
			@PathVariable("userId") @UserIDExisting Long userId) {
		UserStocksResponseDto response = userstockService.getStockHistory(userId);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
}
