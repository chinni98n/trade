package com.haritha.trades.service;

import java.util.Optional;

import com.haritha.trade.BuyRequestDto;
import com.haritha.trade.ResponseDto;

public interface UserStockService {

	public Optional<ResponseDto> buyProduct(Long userId, BuyRequestDto buyRequestDto);
	
}
