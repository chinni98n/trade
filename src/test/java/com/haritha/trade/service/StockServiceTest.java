package com.haritha.trade.service;

import static org.junit.Assert.assertNotNull;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.haritha.trade.dto.StockDetailsDto;
import com.haritha.trade.dto.StockDto;
import com.haritha.trade.exception.RecordNotFoundException;
import com.haritha.trade.repository.StockRepository;
import com.haritha.trade.service.impl.StockServiceImpl;

@RunWith(MockitoJUnitRunner.Silent.class)
public class StockServiceTest {

	@Mock
	StockRepository stockRepository;
	
	@InjectMocks
	StockServiceImpl stockService;
	
	@Before
	public void setup() {
		
	}
	
	@Test
	public void testFindbyName() {
		List<StockDto> stockDtoList = new ArrayList<>();
		StockDto stockDto = new StockDto();
		stockDtoList.add(stockDto);
		Mockito.when(stockRepository.findByName(Mockito.anyString())).thenReturn(stockDtoList);
		assertNotNull(stockService.findByName("ICI"));
	}
	
	@Test(expected = RecordNotFoundException.class)
	public void testFindbyNameNegative() {
		List<StockDto> list = new ArrayList<>();
		Mockito.when(stockRepository.findByName(Mockito.anyString())).thenReturn(list);
		stockService.findByName("IC");
	}
	
	@Test
	public void testFindbyStockId() {
		StockDetailsDto stockDetailsDto = new StockDetailsDto();
		Mockito.when(stockRepository.findByStockId(Mockito.anyLong())).thenReturn(stockDetailsDto);
		assertNotNull(stockService.findByStockId(1L));
	}
	
	@Test(expected = RecordNotFoundException.class)
	public void testFindbyStockIdNegative() {
		Mockito.when(stockRepository.findByStockId(Mockito.anyLong())).thenReturn(null);
		stockService.findByStockId(0L);
	}
}
