package com.haritha.trade.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.haritha.trade.repository.UserRepository;
import com.haritha.trade.service.impl.UserServiceImpl;

@RunWith(MockitoJUnitRunner.Silent.class)
public class UserServiceTest {

	@Mock
	UserRepository userRepository;
	
	@InjectMocks
	UserServiceImpl userService;
	
	@Before
	public void setup() {
		
	}
	
	@Test
	public void testfindUserExistsById() {
		Mockito.when(userRepository.existsById(Mockito.anyLong())).thenReturn(true);
		assertTrue(userService.findUserExistsById(1L));
	}
	
	@Test
	public void testfindUserExistsByIdNegative() {
		Mockito.when(userRepository.existsById(0L)).thenReturn(false);
		assertFalse(userService.findUserExistsById(0L));
	}
	
	
}
