package com.haritha.trade.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.haritha.trade.constants.ApplicationConstants;
import com.haritha.trade.dto.BuyRequestDto;
import com.haritha.trade.dto.ResponseDto;
import com.haritha.trade.dto.UserStockDto;
import com.haritha.trade.dto.UserStocksResponseDto;
import com.haritha.trade.entity.Stock;
import com.haritha.trade.entity.User;
import com.haritha.trade.exception.InsufficientStocksException;
import com.haritha.trade.exception.RecordNotFoundException;
import com.haritha.trade.repository.StockRepository;
import com.haritha.trade.repository.UserRepository;
import com.haritha.trade.repository.UserStockRepository;
import com.haritha.trade.service.impl.UserStockServiceImpl;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RunWith(MockitoJUnitRunner.Silent.class)
public class UserStockServiceTest {

	@Mock
	UserStockRepository userStockRepository;

	@Mock
	StockRepository stockRepository;

	@Mock
	UserRepository userRepository;

	@InjectMocks
	UserStockServiceImpl userStockService;

	BuyRequestDto buyRequestDto;

	Stock stock;

	LocalDate date;

	User user;

	@Before
	public void setup() {
		date = LocalDate.now();
		user = new User();

		buyRequestDto = new BuyRequestDto();
		buyRequestDto.setQuantity(2L);
		buyRequestDto.setStockId(1L);
	}

	@Test
	public void testbuyProduct() {

		stock = new Stock(1L, "ICICI", 100.00, 2L, date);

		Mockito.when(stockRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(stock));
		Mockito.when(userRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(user));

		ResponseDto response = userStockService.buyProduct(1L, buyRequestDto);
		log.debug("expected status {}",ApplicationConstants.SUCCESS_CODE);
		log.debug("actual status {}", response.getStatus() );
		assertEquals(ApplicationConstants.SUCCESS_CODE, response.getStatus());
	}

	@Test(expected = RecordNotFoundException.class)
	public void testbuyProductNegative() {

		Mockito.when(stockRepository.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(stock));
		userStockService.buyProduct(1L, buyRequestDto);

	}

	@Test(expected = InsufficientStocksException.class)
	public void testbuyProductNegative2() {

		stock = new Stock(1L, "ICICI", 100.00, 1L, date);
		Mockito.when(stockRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(stock));
		userStockService.buyProduct(1L, buyRequestDto);

	}

	@Test(expected = RecordNotFoundException.class)
	public void testbuyProductNegative3() {

		stock = new Stock(1L, "ICICI", 100.00, 2L, date);
		user = null;
		Mockito.when(stockRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(stock));
		Mockito.when(userRepository.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(user));
		userStockService.buyProduct(1L, buyRequestDto);

	}

	@Test
	public void testGetStockHistory() {
		user = new User();
		stock = new Stock(1L, "ICICI", 100.00, 2L, date);

		List<UserStockDto> userStockDtoList = new ArrayList<>();
		UserStockDto userStockDto = new UserStockDto(1L, stock, user, 2L, 100.00, date);
		userStockDtoList.add(userStockDto);

		Mockito.when(userRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(user));
		Mockito.when(userStockRepository.findByUser(user)).thenReturn(userStockDtoList);
		
		UserStocksResponseDto response = userStockService.getStockHistory(1L);
		assertNotNull(response);

	}

	

	@Test(expected = RecordNotFoundException.class)
	public void testGetStockHistoryNegative1() {
		user = null;
		Mockito.when(userRepository.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(user));
		userStockService.getStockHistory(1L);
	}
	
	@Test(expected = RecordNotFoundException.class)
	public void testGetStockHistoryNegative2() {
		user = new User();
		log.info(user.toString());
		stock = new Stock(1L, "ICICI", 100.00, 2L, date);

		List<UserStockDto> userStockDtoList = new ArrayList<>();
		Mockito.when(userRepository.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(user));
		Mockito.when(userStockRepository.findByUser(user)).thenReturn(userStockDtoList);
		userStockService.getStockHistory(1L);
	}

}
