package com.haritha.trade.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doThrow;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.haritha.trade.dto.StockDetailsDto;
import com.haritha.trade.dto.StockListResponseDto;
import com.haritha.trade.exception.RecordNotFoundException;
import com.haritha.trade.service.impl.StockServiceImpl;

@RunWith(MockitoJUnitRunner.Silent.class)
public class StockControllerTest {

	@Mock
	StockServiceImpl stockService;

	@InjectMocks
	StockController stockController;

	@Before
	public void setup() {

	}

	@Test
	public void testGetStocksbyName() {
		StockListResponseDto stockList = new StockListResponseDto();
		Mockito.when(stockService.findByName(Mockito.anyString())).thenReturn(stockList);
		ResponseEntity<StockListResponseDto> response = stockController.getStocksbyName("ICI");
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}
	
	@Test(expected = RecordNotFoundException.class)
	public void testGetStocksbyNameNegative() {
		doThrow(RecordNotFoundException.class).when(stockService).findByName(null);
		stockController.getStocksbyName(null);
	}

	@Test
	public void testgetStockDetails() {
		StockDetailsDto stockDetailsDto = new StockDetailsDto();
		Mockito.when(stockService.findByStockId(Mockito.anyLong())).thenReturn(stockDetailsDto);
		ResponseEntity<StockDetailsDto> response = stockController.getStockDetails(1L);
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@Test(expected = RecordNotFoundException.class)
	public void testgetStockDetailsNegative() {
		doThrow(RecordNotFoundException.class).when(stockService).findByStockId((Mockito.anyLong()));
		stockController.getStockDetails(0L);
	}

}
