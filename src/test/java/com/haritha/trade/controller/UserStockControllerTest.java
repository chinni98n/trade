package com.haritha.trade.controller;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doThrow;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;

import com.haritha.trade.constants.ApplicationConstants;
import com.haritha.trade.dto.BuyRequestDto;
import com.haritha.trade.dto.ResponseDto;
import com.haritha.trade.dto.UserStocksResponseDto;
import com.haritha.trade.exception.RecordNotFoundException;
import com.haritha.trade.service.impl.UserStockServiceImpl;

@RunWith(MockitoJUnitRunner.Silent.class)
public class UserStockControllerTest {

	@Mock
	UserStockServiceImpl userStockService;

	@InjectMocks
	UserStockController userStockController;

	ResponseDto responseDto;

	@Before
	public void setup() {
		responseDto = new ResponseDto();
	}

	@Test
	public void testBuyStock() {
		BuyRequestDto buyRequestDto = new BuyRequestDto(1L, 2L);
		responseDto.setStatus(HttpStatus.OK.value());
		Mockito.when(userStockService.buyProduct(Mockito.anyLong(), Mockito.any()))
				.thenReturn(responseDto);
		assertEquals(HttpStatus.OK, userStockController.buyStock(1L, buyRequestDto).getStatusCode());
	}

	@Test
	public void testBuyStockNegative() {
		BuyRequestDto buyRequestDto = new BuyRequestDto();
		responseDto.setStatus(ApplicationConstants.BAD_REQUEST_CODE);
		Mockito.when(userStockService.buyProduct(1L, buyRequestDto)).thenReturn(responseDto);
		assertEquals(ApplicationConstants.BAD_REQUEST_CODE,
				userStockController.buyStock(1L, buyRequestDto).getBody().getStatus());
	}

	@Test
	public void testMyStocks() {
		UserStocksResponseDto userStocksResponse = new UserStocksResponseDto();
		Mockito.when(userStockService.getStockHistory(Mockito.anyLong())).thenReturn(userStocksResponse);
		assertNotNull(userStockController.myStocks(1L));
	}

	@Test(expected = RecordNotFoundException.class)
	public void testMyStocksNegative() {
		doThrow(RecordNotFoundException.class).when(userStockService).getStockHistory(0L);
		userStockController.myStocks(0L);
	}

}
